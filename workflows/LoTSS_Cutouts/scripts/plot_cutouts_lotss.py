import sys
import numpy as np
import pylab as plt
import pandas
from astropy.io import fits
from astropy import wcs
from astropy.nddata.utils import Cutout2D,NoOverlapError
from astropy import units as u
from astropy.coordinates import Angle
from astropy.coordinates import SkyCoord
import pandas as pd


image_input = sys.argv[1]
obj_list_file = sys.argv[2]

# Read in sources to plot from CSV
srcs = pandas.read_csv(obj_list_file)
nsrc = len(srcs)

pos = []
box = []
for i in range(nsrc):
    ra = Angle(srcs.loc[i][1],unit=u.hour)
    dec = Angle(srcs.loc[i][2],unit=u.deg)
    siz = Angle(srcs.loc[i][3],unit=u.arcmin)
    #print(i,ra,dec)
    pos.append(SkyCoord(ra,dec))
    box.append(siz)

#print(pos)

f2 = fits.open(image_input)
w2 = wcs.WCS(f2[0].header)

shape2 = (128,128)

fig = plt.figure(1)


for i in range(nsrc):

    plt.subplot(3,3,i+1)
    cutout = Cutout2D(f2[0].data, pos[i], shape2, wcs=w2)
    gc = plt.imshow(cutout.data,vmin=-3.e-4,vmax=2.0e-3,cmap='afmhot')
    gc.axes.get_xaxis().set_visible(False)
    gc.axes.get_yaxis().set_visible(False)


plt.show()
