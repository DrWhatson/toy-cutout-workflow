import click
import subprocess
import os, sys

@click.group()
def cli():
    pass

@click.command()
@click.option('--survey-dir',default='images',help='Directory for downloaded images')
@click.argument('input', type=click.Path(exists=True), )
def make(input,survey_dir):
    """Process input source list to cutouts"""

    # Run process to find images where sources are located    
    res = subprocess.run(["python","check_LoTSS.py",input])

    # Check process ran okay
    if res.returncode!=0:
        print("Problem running check_LoTSS.py")
        sys.exit(-1)

    # Check survey_dir exists as a directory
    if not os.path.exists(survey_dir):
        print("Survey directory does not exist")
        sys.exit(-1)

    # Try downloading images, also check if already downloaded
    res = subprocess.run(["python","get_LoTSS_images.py",survey_dir])

    # Check process ran okay
    if res.returncode!=0:
        print("Problem running get_LoTSS_images.py")
        sys.exit(-1)

    # Now try and make cutouts
    res = subprocess.run(["python","sort_json.py",survey_dir,input])


cli.add_command(make)

if __name__ == '__main__':
    cli()
