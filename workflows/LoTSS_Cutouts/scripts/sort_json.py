import json
import pandas
import urllib.request
import subprocess
import os, sys

input_json = "data.json"
image_dir = sys.argv[1]
obj_list_file = sys.argv[2]

# Open the JSON file
with open(input_json, 'r') as infile:
    data = json.load(infile)

# Find images again
images = []
for item in data:
    for field in item[1]:
        if field not in images:
            images.append(field)

image_srcs = []
# For each image find image files and list of sources
for image in images:
    # Extract the filename from the URL
    filename = os.path.basename(image)
    filename = urllib.parse.unquote(filename)+'.fits'

    # Set the complete destination path
    destination = os.path.join(image_dir, filename)

    srcs = []
    for item in data:
        if image in item[1]:
            srcs.append(item[0])
    
    image_srcs.append((destination,srcs))

# Read in source list
srcs = pandas.read_csv(obj_list_file,index_col="id")

# Made cutouts for each image
for item in image_srcs:
    image = item[0]
    list_name = image[:-4]+'lis'
    pf = open(list_name,'w')
    pf.write('id, ra, dec, size\n')
    for src in item[1]:
        row = srcs.loc[src]
        pf.write(f"{src}, {row[0]}, {row[1]}, {row[2]}\n")
    pf.close()

    res = subprocess.run(["python","plot_cutouts_lotss.py",item[0],list_name])

