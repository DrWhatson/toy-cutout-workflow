# Toy Cutout workflow

## Description
A very simple python based workflow to extract cutouts from the Lofar Two-metre Sky Survey (LoTSS). Starting with a simple CSV list file of sources in the format of id, ra, dec, size, the workflow will talk to the archive, find which image files are involved, download them and make the cutouts and store them as either png and/or fits

This is more an execise in generating and storing example workflows rather than the cutout service itself.


## Installation
Should be a case of cloning the repo, but a container would probably be better

## Usage
Should be just run the python script with a input list file of object coordinates


## Authors and acknowledgment


## License
MIT License
